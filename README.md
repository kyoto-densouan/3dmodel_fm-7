# README #

1/3スケールの富士通 FM-7風小物のstlファイルです。 スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。 元ファイルはAUTODESK 123D DESIGNです。


***

# 実機情報

## メーカ
- 富士通

## 発売時期
- FM-7 1982年11月
- FM-NEW7 1984年5月

## 参考資料

- [富士通](https://www.fujitsu.com/jp/about/plus/museum/products/computer/personalcomputer/fm7.html)
- [Wikipedia](https://ja.wikipedia.org/wiki/FM-7)
- [IPSJ コンピュータ博物館](http://museum.ipsj.or.jp/computer/personal/0007.html)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_fm-7/raw/3680800b8a2e7c7ce4241cecc0581f28aa14717e/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_fm-7/raw/3680800b8a2e7c7ce4241cecc0581f28aa14717e/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_fm-7/raw/3680800b8a2e7c7ce4241cecc0581f28aa14717e/ExampleImage.jpg)
